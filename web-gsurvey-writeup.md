Previous web experience: I started working as a web developer on campus in
April 2011, so I have almost three years experience developing web
applications.

This assignment was very interesting. I had to consciously try to avoid writing
this assignment the way that we write some of our programs at work. (By using
global variables in the user's session.) Initially, I wrote the survey this
way. I stored the user's answers in a global variable. This worked, until I
realized that if I went back, or duplicated the tab, then my response list
would have extra multiple responses for the same question. By removing the
global answer variable and just passing the answers around inside the ask-
question function, this problem disappeared. This was interesting to me. I
thought about keeping the global variable for answers and just adding extra
code to make sure that answers weren't duplicated, etc, but it would have been
a lot of work. (And I knew it wasn't how I ought to do it.:)) But by just
putting the answers list inside the functions, the problems disappeared.

Similarly, I loved how easy it was to write functions that required user
interaction on the web page. It was so simple and easy to use send/suspend to
just "pause" my program while waiting for the user's input. In my experience,
this has been a bit more work to do any sort of sequencing like this. We
usually have variables to signal what stage of our process the user is in, what
they've done so far, what different values were, etc. We also have to verify
these in between each request to make sure that they're valid and the user
isn't trying to do something they shouldn't be. It feels like a lot of the
problems I'm used to dealing with just disappear when using continuations like
this.
