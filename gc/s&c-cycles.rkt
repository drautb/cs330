#lang plai/gc2/mutator

(allocator-setup "stop-and-copy.rkt" 20)

;; This takes 2 cells
(let ([junk 'junk]))

;; This takes 7 to begin with, two for each primitive
;; and 3 for the cons.
(define l1 (cons 1 2))

;; Now rest of l1 is l1, so the primitive 2 can be collected.
(set-rest! l1 l1)

;; Trigger the collector. There is a cycle in the object
;; structure, but the collector still works. This is because
;; the forwarding pointer is established before the children
;; are copied, thus preventing infinite loops.
(define trigger 'trigger)

(printf "Success!")
