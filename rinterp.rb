# Rudimentary Interpreter written in Ruby 1.9.3
# This program implements a calculator language with the following grammar:
#
#     WAE = number
#         | (+ WAE WAE)
#         | (- WAE WAE)
#         | (* WAE WAE)
#         | (/ WAE WAE)
#         | (with ([id WAE]) WAE)
#         | id
#
# Where number is a natural number and id is s sequence of 
# alphabetic characters, but not with.

# Make it easy to see what strings really are representing
class String
  def is_integer? 
    self.to_i.to_s == self
  end
end

module RudimentaryInterpreter
  
  module Calculator
  
    # Some constants
    ERROR = "ERROR"
    NUM   = "NUMBER"
    ADD   = "+"
    SUB   = "-"
    MULT  = "*"
    DIV   = "/"
    WITH  = "with"
    VAR   = "VARIABLE"
    ET    = "Evaluation completed without solution"

    # Holds variable substitution values
    $variables = {}

    # eat_whitespace : string, number -> number
    # To find the index of the next character in string that isn't whitespace,
    # starting from start_index
    def Calculator.eat_whitespace(string, start_index=0)
      idx = start_index
      while string[idx] != nil &&
            (string[idx] == " " || 
            string[idx] == "\t" ||
            string[idx] == "\n" ||
            string[idx] == "\r")
        idx += 1
      end
      return idx unless string[idx] == nil
      nil
    end

    # test_eat_whitespace : nothing -> nothing
    # To run test for the eat_whitespace method
    def Calculator.test_eat_whitespace
      Testing.test(__LINE__, 
                   eat_whitespace(""),            
                   nil)
      Testing.test(__LINE__, 
                   eat_whitespace(" \r \n \t "),  
                   nil)
      Testing.test(__LINE__, 
                   eat_whitespace("   +  "),      
                   3)
      Testing.test(__LINE__, 
                   eat_whitespace("   +  ", 4),   
                   nil)
      Testing.test(__LINE__, 
                   eat_whitespace("   +  (", 7),  
                    nil)
      Testing.test(__LINE__, 
                   eat_whitespace("   +  (", 4),  
                    6)
    end

    # next_exp : string, number -> string, number
    # To get the string of the next complete expression in string,
    # starting from start_index. Returns that expression as a string,
    # as well as the idx of the first character after it.
    def Calculator.next_exp(string, start_index=0)
      idx = eat_whitespace(string, start_index)
      return [nil, idx] if idx == nil

      exp_str = ""
      if string[idx] == '(' then
        paren_balance = 1
        exp_str += '('
        idx += 1

        while paren_balance > 0 do
          if string[idx] == '('
            paren_balance += 1
          elsif string[idx] == ')'
            paren_balance -= 1
          end
          exp_str += string[idx]
          idx += 1
        end

      else
        while string[idx] != ' ' and 
              string[idx] != ')' and
              string[idx] != ']' and
              string[idx] != nil do
          exp_str += string[idx]
          idx += 1
        end
      end  

      return exp_str, idx
    end

    # test_next_exp : nothing -> nothing
    # To run test for next_exp method
    def Calculator.test_next_exp
      Testing.test(__LINE__, 
                   next_exp(""), 
                   [nil, nil])
      Testing.test(__LINE__, 
                   next_exp(" "), 
                   [nil, nil])
      Testing.test(__LINE__, 
                   next_exp("5)"), 
                   ["5", 1])
      Testing.test(__LINE__, 
                   next_exp("(+ 5 4)"), 
                   ["(+ 5 4)", 7])
      Testing.test(__LINE__, 
                   next_exp("(+ 5 4) 6)"), 
                   ["(+ 5 4)", 7])
      Testing.test(__LINE__, 
                   next_exp("6 (+ 5 4))"), 
                   ["6", 1])
      Testing.test(__LINE__, 
                   next_exp("(- 6 (+ 5 4))"), 
                   ["(- 6 (+ 5 4))", 13])
      Testing.test(__LINE__, 
                   next_exp("1]"), 
                   ["1", 1])
    end

    # parse : string -> hash (Ruby hash) | ERROR
    # To parse a string that contains a valid expression according to the
    # grammar, and return a data structure representation of it.
    def Calculator.parse(string)
      begin
        raise if string == nil or string.empty?
        return { :type => NUM, :val => string.to_i } if string.is_integer?
        return { :type => VAR, :val => string } if string.match(/^[A-z]+$/)

        idx = 0
        if string[idx] == '(' then
          idx += 1
          idx = eat_whitespace(string, idx)

          if string[idx] == '[' then # Brackets for substitution
            idx += 1
            raise unless string.index(']') > 0 and
                                string.index(' ') > 0
            var_name = ""
            while string[idx] != ' '
              var_name += string[idx]
              idx += 1
            end

            result = next_exp(string, idx)
            subs = result[0]
            idx = result[1]

            raise unless string[idx] == ']'
            return { :key => var_name, :value => parse(subs) }
          else  # Regular Operator
            op = ""
            while string[idx] != ' ' and string[idx] != nil do
              op += string[idx]
              idx += 1
            end

            first_result = next_exp(string, idx)
            raise if first_result[0] == nil

            second_result = next_exp(string, first_result[1])
            raise if second_result[0] == nil
            
            case op
              when ADD
                data_struct =  { :type => ADD, :lhs => parse(first_result[0]), 
                                               :rhs => parse(second_result[0])}
                raise if data_struct[:lhs] == ERROR or
                         data_struct[:rhs] == ERROR
              when SUB
                data_struct = { :type => SUB, :lhs => parse(first_result[0]),
                                              :rhs => parse(second_result[0])}
                raise if data_struct[:lhs] == ERROR or
                         data_struct[:rhs] == ERROR 
              when MULT
                data_struct = { :type => MULT, :lhs => parse(first_result[0]),
                                               :rhs => parse(second_result[0])}
                raise if data_struct[:lhs] == ERROR or
                         data_struct[:rhs] == ERROR  
              when DIV
                data_struct = { :type => DIV, :lhs => parse(first_result[0]),
                                              :rhs => parse(second_result[0])}
                raise if data_struct[:lhs] == ERROR or
                         data_struct[:rhs] == ERROR  
              when WITH
                data_struct = { :type => WITH, :lhs => parse(first_result[0]),
                                               :rhs => parse(second_result[0])}
                raise if data_struct[:lhs] == ERROR or
                         data_struct[:rhs] == ERROR
              else
            end

            idx = second_result[1]
            raise unless string[idx] == ')'

            return data_struct
          end
        else # There wasn't an opening paren, but it wasn't a number
          raise
        end

      rescue => e
        return ERROR
      end
    end

    # test_parse : nothing -> nothing
    # To run tests on the parse method
    def Calculator.test_parse
      Testing.test_parse(__LINE__, 
                         nil, 
                         ERROR)
      Testing.test_parse(__LINE__, 
                         "", 
                         ERROR)
      Testing.test_parse(__LINE__, 
                         "42", 
                         { :type => NUM, :val => 42})
      Testing.test_parse(__LINE__, 
                         "(42)", 
                         ERROR)

      Testing.test_parse(__LINE__, 
                         "(+ 4)", 
                         ERROR)
      Testing.test_parse(__LINE__, 
                         "(+ 4 5)", 
                         { :type => ADD, 
                            :lhs => { :type => NUM, :val => 4},
                            :rhs => { :type => NUM, :val => 5}})
      Testing.test_parse(__LINE__, 
                         "asdf3(a+ 4 5)", 
                         ERROR)
      Testing.test_parse(__LINE__, 
                         "(+ 4 5 6)", 
                         ERROR)

      Testing.test_parse(__LINE__, 
                         "(- 8)", 
                         ERROR)
      Testing.test_parse(__LINE__, 
                         "(- 0 5)", 
                         { :type => SUB, 
                            :lhs => { :type => NUM, :val => 0},
                            :rhs => { :type => NUM, :val => 5}})
      Testing.test_parse(__LINE__, 
                         "(- 1 5 9)", 
                         ERROR)

      Testing.test_parse(__LINE__, 
                         "(+ 1 (- 4))", 
                         ERROR)
      Testing.test_parse(__LINE__, 
                         "(+ 5 (- 17 2))", 
                         { :type => ADD, 
                            :lhs => { :type => NUM, :val => 5},
                            :rhs => { :type => SUB, 
                                        :lhs => { :type => NUM, :val => 17 },
                                        :rhs => { :type => NUM, :val => 2 }}})
      Testing.test_parse(__LINE__, 
                         "(+ 5 (- 17 2) 3)", 
                         ERROR)
      Testing.test_parse(__LINE__, 
                         "(- 5 (+ 5 5))", 
                         {:type=>SUB, 
                            :lhs=>{:type=>NUM, :val=>5}, 
                            :rhs=>{:type=>ADD, 
                                      :lhs=>{:type=>NUM, :val=>5}, 
                                      :rhs=>{:type=>NUM, :val=>5}}})

      Testing.test_parse(__LINE__, 
                         "(* 2)", 
                         ERROR)
      Testing.test_parse(__LINE__, 
                         "(* 2 5)", 
                         { :type => MULT, 
                            :lhs => { :type => NUM, :val => 2},
                            :rhs => { :type => NUM, :val => 5}})
      Testing.test_parse(__LINE__, 
                         "(* 1 5 9)", 
                         ERROR)

      Testing.test_parse(__LINE__, 
                         "(/ 4)", 
                         ERROR)
      Testing.test_parse(__LINE__, 
                         "(/ 10 2)", 
                         { :type => DIV, 
                            :lhs => { :type => NUM, :val => 10},
                            :rhs => { :type => NUM, :val => 2}})
      Testing.test_parse(__LINE__, 
                         "(/ 1 5 9)", 
                         ERROR)

      Testing.test_parse(__LINE__, 
                         "(+ 5 (+ 5 (+ 5 (+ 5 5))))", 
                         {:type => ADD, 
                          :lhs => { :type => NUM, :val => 5},
                          :rhs => { 
                            :type => ADD, 
                            :lhs => { :type => NUM, :val => 5},
                            :rhs => { 
                              :type => ADD, 
                              :lhs => { :type => NUM, :val => 5},
                              :rhs => { 
                                :type => ADD, 
                                :lhs => { :type => NUM, :val => 5},
                                :rhs => { :type => NUM, :val => 5}}}}})
      Testing.test_parse(__LINE__, 
                         "(* 2 (- 30 (+ 2 (/ 10 2))))", 
                         {:type => MULT, 
                          :lhs => { :type => NUM, :val => 2},
                          :rhs => { 
                            :type => SUB, 
                            :lhs => { :type => NUM, :val => 30},
                            :rhs => { 
                              :type => ADD, 
                              :lhs => { :type => NUM, :val => 2},
                              :rhs => { 
                                :type => DIV, 
                                :lhs => { :type => NUM, :val => 10},
                                :rhs => { :type => NUM, :val => 2}}}}})

      # Same two as above, but with different ordering
      Testing.test_parse(__LINE__, 
                         "(+ (+ (+ (+ 5 5) 5) 5) 5)", 
                         {:type => ADD, 
                          :lhs => { 
                            :type => ADD, 
                            :lhs => { 
                              :type => ADD, 
                              :lhs => { 
                                :type => ADD, 
                                :lhs => { :type => NUM, :val => 5},
                                :rhs => { :type => NUM, :val => 5}},
                              :rhs => { :type => NUM, :val => 5}},
                            :rhs => { :type => NUM, :val => 5}},
                          :rhs => { :type => NUM, :val => 5}})
      Testing.test_parse(__LINE__, 
                         "(* (- 30 (+ (/ 10 2) 2)) 2)",
                         {:type => MULT, 
                            :lhs => {
                              :type => SUB,
                              :lhs => { :type => NUM, :val => 30 },
                              :rhs => {
                                :type => ADD,
                                :lhs => {
                                  :type => DIV,
                                  :lhs => { :type => NUM, :val => 10 },
                                  :rhs => { :type => NUM, :val => 2 }},
                                :rhs => { :type => NUM, :val => 2}}},
                            :rhs => { :type => NUM, :val => 2 }})

      Testing.test_parse(__LINE__, 
                         "(with [x 1] x)", 
                         ERROR)
      Testing.test_parse(__LINE__, 
                         "(with ([x 1]) x)", 
                         {:type => WITH,
                          :lhs => {
                            :key => "x",
                            :value => {:type => NUM, :val => 1}},
                          :rhs => {
                            :type => VAR,
                            :val => "x"}})
      Testing.test_parse(__LINE__, 
                         "(with ([x 1]) (+ x 1))", 
                         {:type => WITH,
                          :lhs => {
                            :key => "x",
                            :value => {:type => NUM, :val => 1}},
                          :rhs => {
                            :type => ADD,
                            :lhs => {:type => VAR, :val => "x"},
                            :rhs => {:type => NUM, :val => 1}}})
      Testing.test_parse(__LINE__, 
                         "(+ (with ([variable (* 2 2)]) (/ 16 variable)) 1)", 
                         {:type => ADD,
                          :lhs => {
                            :type => WITH,
                            :lhs => { 
                              :key => "variable",
                              :value => {
                                :type => MULT,
                                :lhs => { :type => NUM, :val => 2 },
                                :rhs => { :type => NUM, :val => 2 }}},
                            :rhs => {
                              :type => DIV,
                              :lhs => { :type => NUM, :val => 16},
                              :rhs => { :type => VAR, :val => "variable"}}},
                          :rhs => {:type => NUM, :val => 1}})
      Testing.test_parse(__LINE__, 
                         "(+ (with ([var (* 2 2)]) (/ 16 var)) var)", 
                         {:type => ADD,
                          :lhs => {
                            :type => WITH,
                            :lhs => {
                              :key => "var",
                              :value => {
                                :type => MULT,
                                :lhs => { :type => NUM, :val => 2 },
                                :rhs => { :type => NUM, :val => 2 }}},
                            :rhs => {
                              :type => DIV,
                              :lhs => {:type => NUM, :val => 16},
                              :rhs => {:type => VAR, :val => "var"}}},
                          :rhs => { :type => VAR, :val => "var"}})
      Testing.test_parse(__LINE__, 
                         "(+ (with ([x 2]) (with ([y 3]) (+ x y))) 5)", 
                         {type: ADD,
                           lhs: {
                             type: WITH,
                             lhs: {
                               key: "x",
                               value: { type: NUM, val: 2 }},
                             rhs: {
                               type: WITH,
                               lhs: {
                                 key: "y",
                                 value: { type: NUM, val: 3}},
                               rhs: {
                                 type: ADD,
                                 lhs: { type: VAR, val: "x" },
                                 rhs: { type: VAR, val: "y" }}}},
                           rhs: { type: NUM, val: 5 }})
      Testing.test_parse(__LINE__, 
                         "(+ (with ([x 2]) (with ([y 3]) (+ x y))) x)", 
                         {type: ADD,
                           lhs: {
                             type: WITH,
                             lhs: {
                               key: "x",
                               value: { type: NUM, val: 2 }},
                             rhs: {
                               type: WITH,
                               lhs: {
                                 key: "y",
                                 value: { type: NUM, val: 3 }},
                               rhs: {
                                 type: ADD,
                                 lhs: { type: VAR, val: "x" },
                                 rhs: { type: VAR, val: "y" }}}},
                           rhs: { type: VAR, val: "x" }})
      Testing.test_parse(__LINE__, 
                         "(+ (with ([x 2]) (with ([y 3]) (+ x y))) y)", 
                         {type: ADD,
                           lhs: {
                             type: WITH,
                             lhs: {
                               key: "x",
                               value: { type: NUM, val: 2 }},
                             rhs: {
                               type: WITH,
                               lhs: {
                                 key: "y",
                                 value: { type: NUM, val: 3 }},
                               rhs: {
                                 type: ADD,
                                 lhs: { type: VAR, val: "x" },
                                 rhs: { type: VAR, val: "y" }}}},
                           rhs: { type: VAR, val: "y" }})
    end

    # calc : hash -> number | ERROR
    # To evaluate a data structure representation of an expression in the
    # WAE grammar, and return the value it represents
    def Calculator.calc(data_struct)
      begin
        raise if data_struct == nil or data_struct == ERROR

        case data_struct[:type]
          when NUM
            return data_struct[:val]
          when VAR
            var = $variables[data_struct[:val]]
            raise if var == nil
            return var
          when ADD
            return calc(data_struct[:lhs]) +
                   calc(data_struct[:rhs])
          when SUB
            return calc(data_struct[:lhs]) -
                   calc(data_struct[:rhs])
          when MULT
            return calc(data_struct[:lhs]) *
                   calc(data_struct[:rhs])
          when DIV
            return calc(data_struct[:lhs]) /
                   calc(data_struct[:rhs])
          when WITH
            $variables[data_struct[:lhs][:key]] = 
                                          calc(data_struct[:lhs][:value])
            result = calc(data_struct[:rhs])
            $variables.delete(data_struct[:lhs][:key])
            return result
        end

      rescue => e
        return ERROR
      end

      ET
    end

    # test_calc : nothing -> nothing
    # To run tests on the calc method
    def Calculator.test_calc
      Testing.test_calc(__LINE__, 
                        nil, 
                        ERROR)
      Testing.test_calc(__LINE__, 
                        "", 
                        ERROR)
      Testing.test_calc(__LINE__, 
                        "42", 
                        42)
      Testing.test_calc(__LINE__, 
                        "(42)", 
                        ERROR)

      Testing.test_calc(__LINE__, 
                        "(+ 4)", 
                        ERROR)
      Testing.test_calc(__LINE__, 
                        "(+ 4 5)", 
                        9)
      Testing.test_calc(__LINE__, 
                        "asdf3(a+ 4 5)", 
                        ERROR)
      Testing.test_calc(__LINE__, 
                        "(+ 4 5 6)", 
                        ERROR)

      Testing.test_calc(__LINE__, 
                        "(- 8)", 
                        ERROR)
      Testing.test_calc(__LINE__, 
                        "(- 0 5)", 
                        -5)
      Testing.test_calc(__LINE__, 
                        "(- 1 5 9)", 
                        ERROR)

      Testing.test_calc(__LINE__, 
                        "(+ 1 (- 4))", 
                        ERROR)
      Testing.test_calc(__LINE__, 
                        "(+ 5 (- 17 2))", 
                        20)
      Testing.test_calc(__LINE__, 
                        "(+ 5 (- 17 2) 3)", 
                        ERROR)
      Testing.test_calc(__LINE__, 
                        "(- 5 (+ 5 5))", 
                        -5)

      Testing.test_calc(__LINE__, 
                        "(* 2)", 
                        ERROR)
      Testing.test_calc(__LINE__, 
                        "(* 2 5)", 
                        10)
      Testing.test_calc(__LINE__, 
                        "(* 1 5 9)", 
                        ERROR)

      Testing.test_calc(__LINE__, 
                        "(/ 4)", 
                        ERROR)
      Testing.test_calc(__LINE__, 
                        "(/ 10 2)", 
                        5)
      Testing.test_calc(__LINE__, 
                        "(/ 1 5 9)", 
                        ERROR)

      Testing.test_calc(__LINE__, 
                        "(+ 5 (+ 5 (+ 5 (+ 5 5))))", 
                        25)
      Testing.test_calc(__LINE__, 
                        "(* 2 (- 30 (+ 2 (/ 10 2))))", 
                        46)

      # Same two as above, but with different ordering
      Testing.test_calc(__LINE__, 
                        "(+ (+ (+ (+ 5 5) 5) 5) 5)", 
                        25)
      Testing.test_calc(__LINE__, 
                        "(* (- 30 (+ (/ 10 2) 2)) 2)",
                        46)

      Testing.test_calc(__LINE__, 
                        "(with [x 1] x)", 
                        ERROR)
      Testing.test_calc(__LINE__, 
                        "(with ([x 1]) x)", 
                        1)
      Testing.test_calc(__LINE__, 
                        "(with ([x 1]) (+ x 1))", 
                        2)
      Testing.test_calc(__LINE__, 
                        "(+ (with ([variable (* 2 2)]) (/ 16 variable)) 1)", 
                        5)
      Testing.test_calc(__LINE__, 
                        "(+ (with ([var (* 2 2)]) (/ 16 var)) var)", 
                          ERROR)
      Testing.test_calc(__LINE__, 
                        "(+ (with ([x 2]) (with ([y 3]) (+ x y))) 5)", 
                        10)
      Testing.test_calc(__LINE__, 
                        "(+ (with ([x 2]) (with ([y 3]) (+ x y))) x)", 
                        ERROR)
      Testing.test_calc(__LINE__, 
                        "(+ (with ([x 2]) (with ([y 3]) (+ x y))) y)", 
                        ERROR)

      Testing.test_calc(__LINE__,
                        "(with ([y 3]) (with ([x (+ y 5)]) (+ x 2)))",
                        10)
      Testing.test_calc(__LINE__,
                        "(with ([x 3]) (+ x (with ([x (+ x 5)]) (+ x 2))))",
                        13)
      Testing.test_calc(__LINE__,
                        "(with ([Z 100]) (+ Z (with ([Z 50]) (+ Z 1))))",
                        151)
    end

  end

  module Testing

    require 'pp'

    # Keep track of whether or not any tests have failed
    $all_tests_passed = true 

    # test : number mixed mixed -> nothing
    # To check if an expr is|evaluates to an expected value
    def Testing.test(ln, expr, expected)
      return if !$all_tests_passed
      val = expr
      unless val == expected
        puts "TEST FAILED (#{ln}): #{expr} = #{val}, Expected #{expected}"
        $all_tests_passed = false
      end
    end

    # test : number mixed mixed -> nothing
    # To check if an expr parses to an expected data structure
    def Testing.test_parse(ln, expr, expected)
      return if !$all_tests_passed
      val = Calculator.parse(expr)
      unless val == expected
        pp "TEST FAILED (#{ln}): #{expr} = #{val}, Expected #{expected}"
        $all_tests_passed = false
      end
    end

    # test : number mixed mixed -> nothing
    # To check if an expr calcs to an expected value
    def Testing.test_calc(ln, expr, expected)
      return if !$all_tests_passed
      val = Calculator.calc(Calculator.parse(expr))
      unless val == expected
        puts "TEST FAILED (#{ln}): #{expr} = #{val}, Expected #{expected}"
        $all_tests_passed = false
      end
    end

    # run_all_tests : nothing -> nothing
    # To run all tests on the rudimentary interpreter
    def Testing.run_all_tests
      Calculator.test_eat_whitespace
      Calculator.test_next_exp
      Calculator.test_parse
      Calculator.test_calc

      puts "*** ALL TESTS PASSED ***" if $all_tests_passed
    end

  end

end

RudimentaryInterpreter::Testing.run_all_tests








