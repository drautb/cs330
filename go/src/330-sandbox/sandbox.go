package main

import "fmt"

// func main() {
//     done := make(chan bool)

//     values := []string{"a", "b", "c"}
//     for _, v := range values {
//         go func() {
//             fmt.Println(v)
//             done <- true
//         }()
//     }

//     // wait for all goroutines to complete before exiting
//     for _ = range values {
//         <-done
//     }
// }

// type inter interface {
//     Value() int
// }

// type face interface {
//     Value() int
// }

// type X struct {
//     x int
// }

// func (inst X) Value() int {
//     return inst.x
// }

// func f1(n inter) {
//     fmt.Println("f1 Value: ", n.Value())
// }

// func f2(n face) {
//     fmt.Println("f2 Value: ", n.Value())
// }

// type Parent struct {
//     Val int
//     c   Child
// }

// type Child struct {
//     Val int
// }

// func main() {
//     var myX X
//     f1(myX)
//     f2(myX)

//     var p Parent
//     p.Val = 5
//     p.c.Val = 3
//     fmt.Println("P Val: ", p.Val)
//     fmt.Println("P C Val: ", p.c.Val)
//     fmt.Println("Done.")
// }

func main() {
    f()
    fmt.Println("Done.")
}

func f() {
    // defer func() {
    // 	for r := recover(); r != nil; r = recover() {
    // 		fmt.Println("Recovered in f", r)
    // 	}
    // }()
    fmt.Println("Calling g.")
    g(0)
    fmt.Println("Returned normally from g.")
}

func g(i int) {
    defer func() {
        fmt.Println("First deferred function, recovering")
        fmt.Println("Recover: ", recover())
    }()
    defer func() {
        fmt.Println("Second deferred function, panicking...")
        panic("Second Deferred Panic")
        fmt.Println("After second deferred panic")
    }()
    defer func() {
        // r := recover()
        // fmt.Println("Recovered in third deferred func:", r)
        panic("Third Deferred Panic")
    }()
    fmt.Println("About to hit the first panic....")
    panic("First Panic")
    fmt.Println("About to hit the second panic....")
    panic("Second Panic")
}
