Language Analysis the Second - Go Revisited
===========================================

Ben Draut / CS 330 / Oct. 15, 2013

# Introduction

_Go_ is a relatively new programming language that was developed by Google
beginning in 2007. Initially, it was built for the purpose of streamlining 
the development of Google's in-house systems. It was designed by and 
for people who work with large software systems.

# History

Most programming languages today that qualify as _system_ languages have been
around for a decade or more. Since the birth of these languages, the nature of
computer systems has changed significantly. Computer systems today are modern
marvels of networking and parallel processing. Cloud computing, which is so
ubiquitous today, was just a fantasy twenty years ago. Modern systems deal with
data sets that couldn't even fit on one computer, let alone be processed by
one. Yet they are still built using programming languages that were designed to
build systems that ran this way. Modern programming languages are not typically
adopted as quickly as new hardware is, which can cantribute to a frustrating
developer experience. [1][2]

The speed of modern computers is tremendous, yet development was still
painstakingly slow at Google. [2] The number of systems continued to grow, as
did the frequency of updates, but it seemed slow compile times and dependency
problems would forever be inescapable. Interpreted languages could be used
instead, but they too often sacrificed efficiency for ease of use, requiring
additional time at runtime to provide safety. System engineers at Google, who
worked with large-scale systems every day, decided to start designing _Go_ in
2007 in order to address these concerns.

According to Google, [2] _Go_ was designed from the ground up to be a language
that better served the software engineers in their employment. It would be have
a familiar syntax, be safe, portable, fast to compile, and fast to execute. It
had to support concurrency as a language feature, so as to take advantage of
networked computer clusters and multiple processing cores. It would also have
to be garbage collected, as much to augment developer joy as to reduce
developer error. Lastly, it had to be concise. Everything ought to be defined
just once.

With a vision in mind, Robert Griesemer, Rob Pike and Ken Thompson began
working on _Go_ part-time at Google in 2007. It became a public open source
project in 2009, and _Go 1_ was released on March 28, 2012 [3].

# General Features

## Type System

_Go_ has a somewhat unique type system. It requires explicit type annotations
everywhere except when declaring and initializing new variables simultaneously.
This means that if you want to declare a variable and give it a value at the
same time, a type declaration is not required, _Go_ will infer its type. In
every other case though, such as function definitions, class definitions, or
uninitialized variable declarations, the type system requires type
declarations.

It is interesting to note that in _Go_, there are no implicit conversions for
numeric types. For example, two of _Go_'s primitive types are _int32_ and
_int64_, corresponding to 32-bit integers and 64-bit integers respectively.
Attempting to add two variables one of type _int32_ and one of type _int64_, is
a compile-time error in _Go_. All numeric type conversions in _Go_ must be
explicit. The designers of the language chose not to provide implicit
conversion so as to eliminate confusion for the programmer. When conversions
are implicit, the programmer needs to keep track of the current type in order
to know, for example, how big the value can be or whether or not it is
signed. Requiring explicit conversions make this obvious. [5]

## Scope

_Go_ is lexically scoped, meaning that at any level of the program, only things
that have been declared at that level, or passed to that level, may be used.

## Objects

To the question "Is _Go_ object-oriented?" the creators respond "Yes and no."
Although it does support objects, the word object isn't typically used due to
implications that have been associated with it, but that _Go_ doesn't support.
_Go_ was designed to support both procedural and object-oriented programming
styles, but with some qualifications.

First, _Go_ does not support inheritance, so the inheritance-hierarchy way of
thinking doesn't translate well into _Go_. As such, _Go_ discourages the use of
terms such as "class", "object", and "instance" because they are so closely
associated with these ideas. _Go_ favors instead the terms "type" and "value",
where "_values_ of custom _types_ may have _methods_" [6]. 

Although _Go_ does not support inheritance, sharing implementation of methods
can be achieved by aggregating types. For example, in a language like Java we
could write a _Car_ class that defines a _HonkHorn_ method. We could then write
two other classes _Delorean_ and _Civic_ that inherit from _Car_, thus sharing
the implementation of _HonkHorn_. In _Go_, the same effect could be achieved by
defining the _HonkHorn_ method in a _HornModule_ type, and then adding an
instance of _HornModule_ to both the _Delorean_ and _Civic_ types.

_Go_ does still however support sub-typing. In a language like Java, sub-typing
can be achieved by defning an interface with abstract methods, and then writing
classes that implement the interface. Such classes must then provide a
definition for each method declared in the interface. A popular illustration of
this is with shapes. We could write a _shape_ interface that declares _area_
and _perimeter_ methods. We could then write a _circle_ class and a _square_
class that both implement the _shape_ interface. Each class would provide its
own definition of the _area_ and _perimeter_ methods, thus allowing them to
behave correctly regardless of which is used anywhere that a _shape_ is
required. This example demonstrates nominal sub-typing, meaning that the sub-
typing relationship is defined by naming the interface that a class implements.
In _Go_, sub-typing is structural. This means that the relationship is defined
by whether or not any type implements the methods in an interface.

Using the same example, in _Go_ we would define a _shape_ interface that has
method declarations for _area_ and _perimeter_. Now, any type that provides
_area_ and _perimeter_ definitions, regardless of whether or not the type is
reall a shape, satisfies the interface, and can be used wherever the interface
is required.

Structural typing is not without its own problems unfortunately. As we have
said, it is possible for a type to conform to an interface that doesn't really
describe the type. We could imagine two interfaces, that have similar method
signatures, but represent in reality very different things. A type that
implements all the methods would conform to both interfaces, but it may not
make sense for a value of the type to be used where one of the interfaces is
specified. 

At first, it may appear that without inheritance programmers are forced to
repeat code when defining similar types. Every type of shape in our example
would have to define its own fields for position, or any other attributes
common to all shapes. Isn't that a step backwards? Although _Go_ doesn't
support inheritance, it does resolve some of these issues by still supporting
aggregation and embedding.

Aggregation is familiar to us, and is what one would expect in _Go_. Types may
have other types as fields in their definition, which was briefly touched on
earlier. In our shape example, we could create a Point type, (to represent the
position of a shape) and each of our different shapes would have a Point field.
This works, but is not necessarily the best solution, as it forces message
chaining which can make code more difficult to read. Embedding, on the other
hand, is done the same way as aggregation, but the field declaration is
anonymous. In the shape example, we would declare the Point field in each type,
but not give it a name. This would allow us to use _rect.X_ as opposed to
_rect.Point.X_. Embedded fields behave as if whatever that field contains were
declared in the type to begin with, thus eliminating message chaning. 

The one exception to this rule is when there is a naming conflict between a
field in a type, and a field in one of its embedded types. For example, if we
had a type _Parent_ that embedded the type _Child_, and both _Parent_ and
_Child_ had an _X_ field, then calling _Parent_._X_ would always access
_Parent_'s _X_ field. In order to access _Child_'s _X_ field, you would have to
call _Parent_._Child_._X_. In cases like this, the name of the embedded fields
must be fully qualified.

Another interesting design decision that the designers of _Go_ made was how to
handle the visibility of fields in custom types. Rather than add additional
keywords or require some kind of grouping in the type definition, they decided
to adopt a simple rule: Fields beginning with a lowercase letter will not be
exported, (private) while fields beginning with an uppercase letter will be
exported. (public) The designers say this was a difficult decision to make, but
they are extremely pleased with it now. It greatly increases the clarity of
_Go_ code, as the scope of fields is inherently identified in their name [8].
Personally, I think that this was a good choice. Though it is unconventional, I
think it makes _Go_ code a little easier to read. You know, just by looking a
field's name, what kind of visibility it has.

### Memory Allocation and Garbage Collection

The creators of _Go_ felt that manual memory management required too much
programmer overhead and decided to implement garbage collection in _Go_. This
is especially true when writing concurrent applications that share data and
memory between threads or processes. (_Go_ was designed for exactly these kinds
of applications.) It can be extremely difficult to know for sure when resources
can be released. _Go_'s philosophy is that it's better for the language
creators to implement this once, rather than programmers having to worry about
it for each application. This way, developers can take full advantage of _Go_'s
concurrency mechanisms without needing to worry at all about managing
resources.

Although _Go_ programs do use both a stack as well as a heap for memory
allocation, _Go_ encourages users to think simply about _memory_, rather than
distinguishing between stack space and heap space. To the programmer, memory
should simply be memory, with no extra bookkeeping required. That being said,
they recognize that in reality, whether values are allocated on the stack or
the heap does have a very real impact on performance, and the _Go_ compiler has
been designed to make effective choices about where to allocate memory. For
example, when allocating memory for variables that are declared in a function,
the compiler will attempt to prove that the variable is not referenced after
the function returns. If it can prove this, then it will allocate the variable
on the stack. If it can not, then it will be allocated on the heap. [11] _Go_
also provides a profiler that can be used to improve code in applications where
speed or memory usage are critical factors. [8]

# Special Features

## Package System

One of the primary objectives of _Go_ was efficiency, both in compilation and
execution. _Go_'s fast compilation speeds [12] are primarily due to the nature
of its package system. (In one test at Google, compiling in _Go_ was forty
times faster than compiling the same program in C++.) In C++, build times grow
tremendously as more and more files are added, primarily due to the nature of
the preprocessor and how dependencies are handled. It is common in C++ programs
to include necessary files in each header or source file that requires them.
This can result in the same files being delivered to the compiler over and over
again. Build engineers at Google tracked the workings of the preprocessor in
compiling one of Google's binaries, and discovered that, although all source
files together totaled a mere 4.2 megabytes, over 8 gigabytes were flowing from
the preprocessor to the compiler [9], showing that a huge amount of redunant
work was being done.

_Go_ handles dependencies using its package system, which has been designed
from the ground up to be as efficient as possible. Necessary work is done only
once. Some of the distinctions that set it apart are that unused packages cause
a compiler error, thus ensuring that only what is needed and used is imported,
and that _Go_ packages compile to object files that contain everything required
for client packages. This means that package B may import packages C through Z,
but for package A to use package B, the compiler needs only read package B's
object file. This helps build times significantly.

## Concurrency Mechanisms

Another primary goal of _Go_ was to provided explicit support for concurrency,
thus enabling developers to more easily take advantage of the hardware in
modern multi-core and networked systems. The fundamental concurrency tools in
_Go_'s toolbox are _Goroutines_ and _Channels_.

Goroutines may be thought of as extremely lightweight threads or processes, but
the language manages all the details in the OS. _Go_ has been designed to
support thousands of Goroutines in single programs. The user may spawn
Goroutines at any time, and share the same memory between them all. The way
that Goroutines are built in the language takes care of the work of
distributing their work across the resources that are available. The way that
they are actually used in the code is more natural and easy to think about than
traditional threads as well.

Channels are the mechanisms by which Goroutines coordinate and communicate with
each other. Channels are values that pass around other values. For example, you
could create a channel of integers. Two or more Goroutines may then communicate
by sending values across this channel.

Channels are also first-order values in _Go_, meaning that it is valid to
create a channel of channels. This is incredibly useful for communicating
between Goroutines. Goroutine A may send a request to Goroutine B over a
channel, with a channel embedded in the request that Goroutine B can use to
signal to Goroutine A the response or result of its request. This makes writing
safe remote procedure calls extremely easy.

Because _Go_ also has first-order functions and closures, there are some
interesting mistakes that can be made when using these features with
concurrency mechanisms. Imagine we have a slice of strings: "byu", "cs", and
"330". We could loop through the slice, print each string out, and see the same
list, just as we would expect. Now let's do something that seems silly for this
case, but could apply to a similarly framed real-world situation. Rather than
just printing each string, let's call an anonymous function that prints it for
us. The result is the same. Now, rather than calling the anonymous function
directly, let's call it with the go keyword to make it a Goroutine. If you use
the loop variable to print each string, this will output "330", "330", and
"330". This happens because all three closures in the anonymous functions share
the same loop variable. In most situations (single core machines), the launch
of the Goroutines will be deferred until the loop completes. When the loop
completes, the loop variable will have the value of the last item in the list,
thus causing the repetition in the output. To get around this, a new variable
must be created in each iteration of the loop and given to each function. (This
is a common idiom in _Go_) So, _Go_ does provide excellent tools for
concurrency, but care must be taken when using them.

_Go_ also ships with a tool that is designed to discover some of these errors,
including the one we mentioned previously. The tool, called _go vet_, performs
a static analysis of the code and reports pontential mistakes. When run on the
previous example, it reports: _range variable v enclosed by function_, thus
alerting us to the problem. 

## Error Handling

_Go_'s philosophy towards error handling takes a somewhat unique approach. _Go_
does not provide support for assertions or exceptions. The designers felt that
exceptions made it more difficult to follow the code, and that they were too
frequently misused. (Meaning that they were used to handle errors, rather than
situations that were truly exceptional.) The felt that assertions were used as
a crutch to avoid thinking about proper error handling. [13]

_Go_'s philosophy is that errors should always be properly handled. _Go_ makes
it extremely easy to deal with errors. The majority of functions return both a
result and error value, indicating whether or not to trust the result. Although
a tiny bit of extra code is required following each call to handle possible
errors, it simplifies the code overall by reducing the number of logical paths
the code could follow. (The the extra code is really no more than exception
code would be.)

Although _Go_ does not support exceptions in the traditional sense, it does
have support for similar mechanism, known as _panicking_. Most developers are
comfortable with the try-catch-finally model. Code that could throw an
exception is surrounded by a try block, followed by a catch block (blocks) to
catch and handle different exceptions that could occur, followed by a finally
block that is guaranteed to be executed, typically used for cleanup. Developers
of _Go_ felt that this model was difficult to follow when reading the code, and
had become abused. Too often developers were using exceptions for control flow
and dealing with simple errors. Truly exceptional situations are far rarer that
most developers think. _Go_ uses a defer-panic-recover model. Calling defer on
a function will push it onto a stack, from which all functions are popped and
executed when the function ends, or if a panic occurs. Panics may be initiated
by the language for things like trying to access an invalid array index. When a
panic is triggered, all deferred functions are executed up the call stack until
either the program terminates, or recovers. Deferred functions may call
recover, which will stop the panicking, return the value of the last panic, and
resume normal execution.

This model has several benefits. First, panicking aside, being able to defer
functions helps improve the readability of the code and makes life easier on
the developer. Whenever the developer creates a resource or does any kind of
set up, they can think about the tear down at the same time, and just defer it.
This makes it really easy to make sure that things get cleaned up. As soon as
you open a file, defer the function that will close it. Done. If a panic does
occur, you're still guaranteed that it will get cleaned up. It also simplifies
the logic of handling exceptional situations.

There are some interesting issues that this model introduces as well however.
What if a function panics, and then one of its deferred functions panics too?
Not much changes. All deferred functions are still executed up to the point
where they panic, at which point the next deferred function will be executed,
or execution will jump up the call stack. This can cause problems though, as
recover only remembers the reason for the last panic, not the whole chain, so
it can be easy to obscure the real reason that panics happen. For example, if
we have a function that causes a panic because we tried to access an out-of-
bounds array index, execution will jump to the deferred functions where we do
some cleanup. Now, let's imagine that another panic happens because something
went wrong during the cleanup. There are a few possible scenarios: First, we
don't do any recovery, and both panics are displayed in the program output. We
become aware of both, which is good! Second, we attempt to recover often, so
we're able to detect and recover from the first panic before the second
happens, which is also good! Third, we attempt to recover, but not often. Our
program may recover before it terminates, and it looks like the cause was the
cleanup panic. We would have no idea that the panic in the original function
ever happened. If programmers aren't careful about how and where they do panic
recovery, it could make debugging significantly more difficult.

Another aspect of this model that is worth considering is how recoveries
actually work. Recover may only be called in a deferred function. If it is
called anywhere else, it will just return nil, and have no effect on the
program. Deferred functions are only executed when execution is leaving a
function though, so there is no way to return to the code in a function after
recovering from a panic. With the try-catch model, it is normal to simply
surround potentially problematic segments of code, thus allowing you to protect
the program from terminating, while still allowing you to finish the code
block. In _Go_, this isn't possible. As soon as panic is called, the rest of
that function is gone. You can recover in the first deferred function, but
execution will continue at the next level up the call stack. This means that
potentially important operations could be left undone, corrupting the state of
the program. For example, let's imagine a function that removes a URL from a
queue where it was waiting, and then indexes the page at that URL. The function
could remove the URL from the queue, but then a panic could occur before the
indexing happened, thus that page would be lost. There are arguments for and
against this. My opinion is that it encourages programmers to write concise
functions, which simplify code and improve clarity.

## Safety Features

The designers of _Go_ implemented several features to eliminate subtle errors
that can be frequent in C or C++ programs. A few of these are that all
variables are guaranteed to be initialized to 0 or nil, if not explicitly
initialized on creation, and that no pointer arithmetic is allowed [10]. 

Another interesting feature is how _Go_ avoids the problem of returning
references to local variables. For example, in C, we could declare a variable
_X_ in a function call and then return its address from the function. When the
function returns however, the local variable isn't guaranteed to still be at
that address. Since the function returned, its stack frame has been discarded,
and another stack frame could replace it, overwriting the value we had at that
address. (GCC will warn you if you attempt this however.) In _Go_, you don't
need to worry about this. _Go_ will preserve stack variables that have had
their addresses taken so as to prevent this problem.


# Sources

[1]: http://golang.org/doc/faq#Origins
[2]: http://talks.golang.org/2012/splash.article
[3]: http://golang.org/doc/faq#What_is_the_status_of_the_project
[4]: http://golang.org/doc/faq#generics
[5]: http://golang.org/doc/faq#conversions
[6]: Summerfield, _Programming in Go_, 254
[7]: http://asserttrue.blogspot.com/2009/02/inheritance-as-antipattern.html
[7]: http://talks.golang.org/2012/splash.article#TOC_11.
[8]: http://golang.org/doc/faq#garbage_collection
[9]: http://talks.golang.org/2012/splash.article#TOC_5.
[10]: http://talks.golang.org/2012/splash.article#TOC_12.
[11]: http://golang.org/doc/faq#stack_or_heap
[12]: http://talks.golang.org/2012/splash.article#TOC_7.
[13]: http://golang.org/doc/faq#exceptions
