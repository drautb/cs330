>L22-24 - This is not quite true. PLs have evolved, but the evolved PLs 
are not widely used the same way new hardware is widely used 

>L27 - citation needed 

>L30 - I disagree that so-called "interpreted" languages often 
sacrifice safety. They are typical more safe than, say, C. What do you 
mean here? 

>[L34 - This is not a critique. If you lay out these claims in the 
introduction, you should make sure the rest of the paper supports 
them.] 

>mean by dynamic types? Generic types? Why should we care? Also, it is 
irrelevant to talk about languages that don't exist. 

>L55 - This is a confused hodgepodge. First, you do not add variables 
to numbers. You add values and variables store values. So, what are 
you talking about? Second, you should not just say this, you should 
analyze and discuss it. 

>L66 - Compiler or run-time system? 

>L68-69 - You need to back up these claims that they complicate the 
compiler and are not portable. 

>L69-71 - The idea that the compiler is more important than the 
programmer appears to go against the principles you laid out 
earlier. You should reflect on this. 

>L77 - What do these things mean and why does it matter? 

>L93 - This paragraph is too shallow. Also, it confuses the purpose of 
inheritance. You need to discuss the different between the sub-typing 
relationship that interfaces provide [which is what you talk about] 
and the implementation sharing that inheritance provides [which is 
what you claim to talk about]. Your mind is warped by Java where these 
two things are commingled. 

>L112 - This is not a real term. And that's not what people normally 
mean by it anyways, because in Go it is actually checkable. 

>L114 - Defend the claim that inheritance has problems 

# Not sure what this problem is, I could make two inferfaces with the same method signature, and both worked just fine.
>L128 - No evidence for this claim. Furthermore, you should talk about 
the problem that one method name can only be used once for all 
interfaces. 


>L145 - grammar 

>L145-146 - Why does it reduce clarity? What is clarity? 

>L150-152 - What does this mean? When would you need to do this? Is 
there any downside to this system? 

>L154 - And what do /you/ think about this? 

>L168 - Parenthetical is not a sentence 

>L179 - How are these intelligent decisions made? 

>L182 - What constitutes properly? 

>L184 - Not a sentence and what do you think about this? 

>L192 - No evidence of actually being fast 

>L191 - This whole paragraph needs to go deeper into what the supposed 
problems and solutions are. 

>L215 - Why are they so much better? Is it engineering or something in 
the language semantics? 

>L230 - No evidence of such hailing 

L220 - You need to cite CML 

>L249-250 - This tool sounds interesting. What are the mistakes and how 
does it catch them? 

>L256 - What is the basis of this argument? 

>L259 - Simply what? Also, not a sentence 

>L262 - Back up claim of verbosity 

>L272 - Why did they feel this way? 

L284 - You should talk about the gross pattern of trying to defer more 
than a single function call 

L300 - This example is described in a messy way with a lot of 
colloquial writing 

L321 - What's the consequence of it not being possible? [You can 
simulate it by splitting a function in two... but that's ugly.] 

L329 - Improve relative to what? 

L330 - Why are these things good for safety? 

L335 - Messy explanation and not really true 

>L338 - I thought you said earlier that it doesn't have the difference 
between stack and heap? 
