Analysis: Garbage Collection
============================

Ben Draut / CS 330 / Oct. 22, 2013

# Problem

Computers have finite amounts of memory. As a result, creators of programming
languages must decide whether their language will manage memory itself, or pass
this responsibility on to the programmer. We call languages that manage memory
themselves garbage collected, while languages that require the programmer to
manage memory use manual memory management. In this paper, I will give an
overview of the benefits that follow as well as the trade-offs that must be
made for both options.

# Evaluation Criteria

In order to evaluate the _goodness_ of any single memory management approach, I
will establish the criteria that will be used later to measure each approach. I
will start by defining a few terms that will aid in my discussion of these
criteria. An allocated block of memory may be in one of three states: active,
inactive, and unnameable. Active memory refers to memory that is being actively
used by the program, meaning that the program will use that memory again, even
if it is not using it at any given instant. Inactive memory refers to memory
that the program will not use again. Unnameable memory refers to memory that
the program cannot use again, because it cannot reference that memory. It has
somehow lost its name. In addition, I will use the term object to refer to a
block of allocated memory.

I will use the following evaluation criteria: First, there will always be some
cost associated with the allocation and deallocation of memory. I will not use
any special term when referring to these costs. Second, there is latency.
Latency is the amount of time that passes between the moment when an allocated
block of memory becomes inactive, and the moment when that block is
deallocated. Third is efficiency. Efficiency refers to the overhead required
for an approach to perform any necessary bookkeeping. Fourth, I will look at
the soundness of each approach. An approach is considered sound if it is
guaranteed to never deallocate a block of memory before it becomes inactive.
Fifth, I will look at the completeness of each approach. An approach is
complete if it is guaranteed to deallocate all inactive and unnameable memory.

These criteria are closely related and there are trade-offs between them.
Depending on the situation, some may be more desirable than others. If I were
designing a language for an embedded system with a very small amount of memory,
ensuring completeness would be the most desirable. However, this would incur
some cost elsewhere.

I will discuss manual memory management implementations first, followed by
garbage collection implementations.

# Manual Implementations

## Purely Manual

Purely manual memory management is when the programmer does all of the memory
management. The programmer is alone responsible for making sure that memory is
allocated and deallocated at the right time. 

The cost of allocating and deallocating memory is small for this approach,
typically log(n) as it must traverse a binary tree of some kind where it keeps
track of unallocated memory blocks. (I was unable to find a source for this,
but we discussed it in class.) Assuming that the programmer makes no mistakes,
latency for this approach is zero since the programmer will deallocate memory
the moment that it becomes inactive. It is also perfectly efficient because
there is no additional bookkeeping to be done for memory management. Lastly, it
is also sound and complete because the programmer will never forget to
deallocate inactive memory, nor deallocate active memory.

Purely manual memory management therefore has the potential to be the best all-
around option, and I will use its metrics as a benchmark for other approaches
discussed later. If the programmer makes no mistakes, there are no downsides.
The only cost incurred is the cost of allocating and deallocating memory, which
cannot both be removed. (An approach may remove the cost of deallocating memory
by simply doing nothing, but this compromises completeness.) Programmers do
however make mistakes, therefore these claims must be proven on a per-program
basis in practice, and cannot be generalized across all programs for this
approach.

## Reference Counting
  
Reference counting is a manual memory management strategy in which the
programmer implements features in his program in order to automate memory
management. (I consider this approach to still be manual because the
responsibility still lies with the programmer.) It works like this: Every
object is given an additional value that represents the number of references to
that object in existence. For example, when and object is allocated, the value
becomes 1. If the object reference is assigned to another variable, the value
is increased 1. If one of these references goes out of scope and is destroyed,
the value is decreased by 1. Whenever the value is decreased, a check is made
to see if the value is 0. If it is, it means that no references exist to this
object, and it may be safely deallocated. The programmer is responsible for
implementing this functionality, but once it is in place, memory management has
become automatic. The programmer may create new objects as he would normally,
but now he need not concern himself with deallocating them, thus hopefully
eliminating errors that would be made in a purely manual memory system.

The cost of allocating and deallocating memory is slightly higher when doing
reference counting due to the small piece of additional memory that must be
allocated to hold the reference count for each object. The size of this
additional memory depends on the number of references that may exist to an
object. In reference counting, latency is extremely low because objects are
deallocated as soon as the last reference to them is destroyed, or goes out of
scope. Formally, latency is not quite zero because an object may become
inactive long before its last reference is destroyed. Reference counting
deallocates objects when they are about to become unnameable, not when they
become inactive. It is also less efficient because there is extra overhead
associated with incrementing, decrementing, and storing the reference counts.
It is also sound, but incomplete however. Counting references ensures that an
object will never be deallocated prematurely, but it does not guarantee
completeness. I will discuss this in more detail later.

On paper, reference counting is purely worse than purely manual memory
management. However, it significantly reduces the chance for programmer error
by automating the process of deallocating memory, which is a benefit that may
be worth the cost.

Reference counting is not foolproof however, as it is unable to deal with
cyclic references. If two objects have references to each other, then reference
counting will never deallocate them because there will always exist at least
one reference to the other. This is why reference counting is incomplete.
Cyclically referenced objects will never be deallocated. We can imagine a few
ways of circumventing this problem, perhaps the simplest of which is just to
forbid such references. If cyclic references cannot exist, then reference
counting is complete. Another option is to not count any references that would
complete a cycle. This method allows circular references to exist, but prevents
them from locking up memory.

Lastly, it is important to note that reference counting can significantly hurt
performance in multi-threaded environments. The reference count values must be
correct in order to preserve the integrity of the algorithm. This requires
synchronization of all threads across which objects are shared, and the
alteration of the reference count to be an atomic operation, both expensive in
multi-threaded environments [1].

# Garbage Collection Implementations

Garbage collection is implemented at the language level, distinct from the
program itself. In our discussion of garbage collection implementations, I will
refer to the program as the mutator, and the garbage collector simply as the
collector.

## Mark and Sweep

Mark and sweep is a garbage collection implementation that values space over
time. It works by periodically traversing the tree of all nameable objects,
marking them, and then sweeping over all of memory deallocating any objects
that are not marked. The collector starts its traversal with the root set of
objects, or the set of objects that last the entire lifetime of the mutator. It
looks inside each of these objects for other objects, marking each of them as
it goes, until it can't find anymore. When this is done, the mark phase has
completed, and the sweep phase begins. During the sweep phase, the collector
examines all program memory. Whenever it finds an unmarked object it
deallocates it, while the marked objects are left untouched. Once all program
memory has been examined, the sweep phase is complete.

Similar to reference counting, the cost of allocating and deallocating memory
is slightly augmented when using mark and sweep. This is because additional
space must also be allocated here in order to mark each object. Mark and sweep
requires only a single bit however, while reference counting required more. The
latency of mark and sweep depends on the frequency with which the collector is
run. If the collector is run more frequently, latency will decrease. However,
it cannot be zero as the collector must always do some work before any memory
is deallocated. In terms of efficiency, mark and sweep is the most expensive
approach examined so far. The mark phase requires time proportional to the
number of reachable objects, and the sweep phase requires time related to the
size of memory. Lastly, mark and sweep is guaranteed to be sound and complete.

Mark and sweep depends on a few things being true. First, it assumes that no
pointers exist that it did not create. If a programmer manually creates a
pointer to an object, it is possible that the collector will deallocate that
object before the programmer is done with it because it was unaware that the
pointer existed. Second, mark and sweep requires the collector to understand to
some degree the structure of objects in the mutator. The collector must have
some way of knowing which parts of an object are pointers to other objects in
order to properly traverse the object tree and mark them. This increases the
interface between the mutator and the collector.

Though mark and sweep is less efficient than the manual implementations
previously examined, it does have the added benefit of being able to run in
parallel with the mutator. If the collector is run separately, everything
remains mostly the same. The potential difference is that the collector may
mark an object, and the object may become inactive before the collector sweeps.
This means that the collector will neglect to deallocate an object that is
inactive. We realize however, that this object will be deallocated the next
time the collector runs, thus preserving completeness. The potential cost of
this is slightly increased latency.

## Stop and Copy

Stop and copy is an alternative to mark and sweep favoring time over space.
First, it divides the program memory into two halves, and chooses one half to
use. Whenever a new object is created, it simply allocates it at the next open
spot in memory. This continues until there is not enough space to satisfy the
request of the mutator, so the collector runs. The collector traverses all
objects reachable from the root set, copying them to the half of memory that
wasn't in use, and inserting forwarding pointers in the first half of memory.
The second half now becomes the current memory pool, and execution resumes.

Both allocating and deallocating memory in stop and copy are extremely fast.
Allocating memory is linear, as it simply moves the pointer for the next
available memory location to the end of the allocated object. Deallocating
memory is free. Deallocation is never done explicitly, it happens naturally as
active objects are moved back and forth between halves of memory, and inactive
objects are overwritten. Latency for stop and copy varies, depending on the
frequency with which the collector runs. Stop and copy is also sound and
complete. In terms of efficiency however, running the stop and copy collector
is somewhat expensive. It requires at least enough time to traverse all the
active objects, but it must also copy each active object to the other half of
memory. Therefore it requires time proportional to the number of active
allocated objects. The benefit is that allocating memory is extremely fast. If
the collector seldom needs to be run, or if it can be run while the mutator is
blocked for some other reason, then the delay may be unnoticeable.

We could imagine programs in which a large object, or a few large objects, last
for the entire lifetime of the program. Repeatedly copying these objects back
and forth between memory halves is obviously wasted work if they will last for
a long time. A possible solution to this problem would be to have another area
of memory for these long-lived objects, so as to avoid repeatedly copying them.
This alternate area could be collected using mark and sweep rather than stop
and copy. This would eliminate the wasted copying, but how would we determine
if an object is big enough, or will be active long enough, to justify being
allocated in this space? Generational garbage collectors attempt to answer
these questions.

## Generational Garbage Collectors

Generational garbage collectors try to put together the best of both mark and
sweep, and stop and copy. Memory is divided into two areas. One area (I will
call it S.) will be collected using stop and copy, and is the default
allocation location. The other area (I will call it M) will be collected using
mark and sweep. When the stop and copy collector runs, any active objects are
copied to area M, rather than to a second stop and copy area. It is hoped that
in this way, objects that persist will be managed efficiently by leaving them
in area M, and objects that become inactive quickly are deallocated efficiently
by leaving them in area S.

Generational garbage collectors introduce other challenges. They must take care
when doing collection to preserve the integrity of references that cross over
between the two areas. Additional bookkeeping is necessary to do this, but it
may be worth the efficiency benefits gained from this approach.

# Sources

[1]: http://en.wikipedia.org/wiki/Garbage_collection_(computer_science)#Referen
     ce_counting
