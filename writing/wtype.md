Analysis: Type Systems
======================

Ben Draut / CS 330 / December 3, 2013

# Introduction

Some programming languages provide type systems as a means of helping the
programmer. Type systems provide guarantees about program behavior and state.
This removes a burden from the programmer's shoulders, allowing them to focus
directly on the software they are writing.

# What is a Type System?

In order to talk about type systems, we first need to establish what a type is.
A type is an invariant, or something that is guaranteed not to change. For
example, we can imagine a typed programming language that supports an integer
type or invariant. Anytime you, the developer, use something that has been
declared to be of this integer type, you can know certain things about the
value you are dealing with. You may know that it is a whole number that falls
in some range, you may know that it uses a certain amount of space in memory,
and you may know which operations can be performed on it. These are all ideas
that are associated with a specific type. Some other perhaps familiar examples
would be floating point or double precision types, string types, boolean types,
and user-defined types.

It is important to note that types are not required for the computer to run the
software we write. There are programming languages that do not have a type
system, such as C. We could also imagine a programming language in which there
is no way to declare a variable of any type. Rather, all we would have is a
function to request a number of bytes in memory. The burden would be completely
on the programmer to make sure that values were stored properly in memory, that
the necessary operations were defined, and that no mistakes were made! This
would be an enormous task, with greater chance for error.

Having types in our languages is very useful. It raises the level of
abstraction, thus making it easier to think about the work being done by the
program. This in turn helps us to make sure the work is being done correctly,
and minimizes error. It is the job of the type system to enforce the invariants
that are promised by the language, helping programmers think at a higher level,
and preventing them from making mistakes by working at a lower level.

# What Do Type Systems Do?

Before discussing the kinds of errors that a type system can detect, it can be
helpful to talk about different kinds of errors that a program may have. At one
end of the spectrum, we have errors that the computer cannot detect; meaning
that the program is valid, but it doesn't do what the developer intended. At
the other end, we have syntactic errors. Errors in the actual source code text
of the program that make it impossible for the computer to understand it.
Somewhere in between are logic errors, such as dividing by zero or
dereferencing a null pointer. These errors don't cause the compiler to reject
the program, but at runtime their behavior is undefined.

What kinds of errors would a type system detect? To better understand this
point, we need to mention a few things about context. At the very beginning of
the process of running a program is parsing. Paring occurs in a context free
environment. This means that the parser doesn't have any conception of what is
actually happening at any point in the program. All the parser cares about is
that the text of the program conforms to the grammar of the language. On the
other hand, the full context of the program isn't known until the program is
actually running. In between these two points is an area that is described as
context sensitive, a point where at least some context can be determined. This
is where a type system can detect errors, in between the syntactic errors that
the parser recognizes, and the logic errors that can normally only be found at
runtime.

For example, we can imagine a programming language in which the addition
operator is defined only for numbers. We could write a program that calls the
addition operator with a number on the left hand side, and a function call that
we know returns a boolean on the right hand side. The parser would not complain
as both the number and function call are valid expressions in the language. At
some point, the type system would check the call in question, realize that we
are attempting to add something that is not a number, and signal an error.

It is interesting to note that type systems vary greatly in power, and thus the
error spectrum is subject to change depending on the type system of the
language at hand. Common type systems typically enforce a rather small set of
general invariants, such as numbers or strings. Other type systems are much
more powerful, such as the type system used in Coq or Typed Racket. More
powerful type systems provide a greater number of more specific invariants,
giving the developer additional precision when specifying the state of the
program. Some examples of such invariants are an integer between -10 and 10
that is not 0 or odd, as opposed to just an integer, or a list of numbers that
is always sorted, as opposed to just a list of numbers. Returning to our
discussion of errors, we could imagine a type system that is so powerful, it
encompasses all logic errors. The invariants themselves could ensure that a
number never becomes 0, thus preventing division by zero, or that a pointer
never becomes null. Building such a type system however, though possible, is a
daunting task, and research is ongoing to prove or disprove the utility of such
systems.

# Type System Metrics

Type systems are measured by whether or not they are sound. In order to
understand what sound means for type systems, we will discuss a little bit
about how type systems work. Type systems have a piece called a type checker
that makes predictions about the programs they analyze. For example, the type
checker may analyze an expression in a program and determine that the
expression is a numeric type. The means that the type system is predicting that
when the expression is evaluated at runtime, it will produce a number that
conforms to the invariants for a number. Could the type checker be wrong? The
type checker does not actually run the program, so we cannot assume that it is
correct.

A sound type system is one that has been proven to be correct, meaning that its
predictions about the program are always true. Generally speaking, we say that
for any expression _e_, the type checker will produce a type _t_. When _e_ is
actually run, it will produce a value. In a sound type system, this value will
always also be of type _t_.

Type system soundness is typically proven by proving that the type system has
two properties, namely progress and preservation. Progress is the guarantee
that if a program passes the type checker, it can perform a single step of
execution. Preservation is the guarantee that the value resulting from
evaluating a typed expression will have the same type as the expression had
before evaluation. Soundness can be proven by combining these two properties,
beginning with progress. Progress tells us that a type checked expression can
perform one step of evaluation. Preservation tells us that the resulting type
is the same as before. Therefore, we can again apply progress and perform
another step of evaluation, and the cycle repeats until there are no more
expressions to evaluate, and the program value remains. This completes the
soundness proof.

The soundness proof is correct, but there are some additional possibilities
that we need to consider. First, the proof above implies that the program
evaluates to a value, or terminates. However, we can imagine programs that we
don't want to terminate, such as web servers. These programs may pass the type
checker, but never actually produce a value. Although they don't formally fit
the definition, the type system can still guarantee that they are performing
sensible computations as they run.

In addition, there may still arise errors in our program that we mentioned
earlier, such as dividing by zero, or attempting to dereference a null pointer.
In most type systems, such operations cannot be typed, so the type system
specifies some set of acceptable runtime errors that may occur to cover these
cases. These two possibilities mean that there are really three possible
outcomes when a type checked program is run: It may terminate resulting in a
value of the predicted type, it may never terminate, or it may terminate
prematurely as the result of a runtime logic error.

# How Do Type Systems Work?

We discussed previously how type systems determine the type of programs, which
can be guaranteed by proving the soundness of the type system. How does the
type system know which types it has to deal with? The language must define
these types, how they can be represented, and how they can be used.

Once the types have been established, how the does the type system know whether
given expressions conform to a particular type? There are two primary ways this
is done. First, a language may require the developer to explicitly annotate the
type new variables or expressions. In Java for example, the programmer cannot
simply start using a variable named _foo_. They must first declare _foo_ to be
of some type, such as Number, String, or some class. The type checker can then
use these annotations to ensure the validity of the program.

The second method is by type inference. A language may allow the programmer to
write a program that does not contain explicit type annotation, and then have
the type system infer the type of each expression based on context before
running the type checker. At the least, type inference saves the developer some
typing, but a more significant benefit is that type inference also ensures that
the most polymorphic types may be used in every expression. I will go into
further detail on type inference later.

# Classifying Type Systems

So far, I have only talked about type systems doing type checking prior to the
actual program execution. If a program fails to type check, then it is rejected
as a program and cannot be run. Are there other kinds of type systems? I have
often heard many programming languages described as being "statically typed" or
"dynamically typed." How do these terms align with our discussion of type
systems so far?

To say that a language is statically typed is synonymous with saying that it
has a type system. To say that a language is dynamically typed does not make
sense. When people say that a language is dynamically typed, what they may mean
is that the language provides safety. A language that provides safety is a
language that makes sure your program will not perform any incompatible
operations. This is checked at runtime however, while type systems check this
before the program is ever run.

# Type Inference

As was mentioned previously, type inference is a process in which a type
system may infer the type of a program that lacks explicit type declarations.
This is desirable because it removes the need for the programmer to annotate
types, but also because it ensures that the most polymorphic types may be used
throughout the program. 

When I say that type inference ensures that the most polymorphic types may be
used, it means that the type system enforces the loosest constraints possible
on every expression. Lets look at the identity function as an example. In a
typed language that requires type annotations such as Java, we would have to
write several versions of the identity function, one that accepts and returns
an integer, a float, etc. (This problem could be avoided in Java by using
subtypes, but this is not true of all languages.) If type inference were used,
we would need only write a single version of the identity function. The type
system would infer that it returns a value of the same type that it accepted,
and thus would allow it to be invoked with arguments of any type.

The type inference process works in two steps. The first step is to analyze
the program and generate a set of type constraints. The second step is to solve
these constraints to yield a final type for the program.

Constraint generation consists of creating a unique label for each expression
in the program, and then creating rules that describe the relationships between
these expressions. The basis for this comes from the structure of the language
itself and the rules that the language designers decided on. For example, a
language may treat all numeric characters are of a number type, that the left
and right hand side of an addition operation must also be of number type, and
that the result of the addition operation must be of number type. Furthermore,
anywhere that an addition operation is used, there will be a constraint saying
that the expected type must match the type of the addition operation. These
constraints completely describe the allowable types in each expression.

Once the constraints have been generated, the solving process begins. It turns
out that the  solving process is analogous to the mathematical process of
Gaussian elimination. In short, Gaussian elimination is the process by which
solutions to linear system of equations can be determined. The set of
constraints that are generated in type inference correspond to a linear system
of equations.

When the solving process begins, we have the set of generated constraints named
_C_, and an empty substitution set named _Θ_. Each constraint in _C_ is
examined, and five rules govern what is to be done. They are defined as
follows:

1. If the left hand side of the constraint is the same as the right, then we
simply discard the constraint. This is because such a constraint contains no
information. It is analogous to an equation in a linear system such as x = x,
which is ignored in Gaussian elimination.

2. If the left hand side of the constraint is a label, then we add the
constraint to _Θ_, and replace all occurrences of the label in _C_ and _Θ_ with
the right hand side of the constraint.

3. If the right hand side of the constraint is a label, then we reverse the
constraint so that the label is on the left hand side and add it to _Θ_, and do
the same replacement as in rule two.

4. If the left and right hand side of the constraint are matching constructor
types, then we don't add anything to _Θ_ or do any replacements. Instead, we
add additional constraints to _C_ that will be handled later. For each argument
_left-arg_ in the left hand side constructor, we add a new constraint of the
form _left-arg_ = _right-arg_, where _right-arg_ is the argument to the right
hand side constructor in the same position. This corresponds to the row
reduction that takes place in Gaussian elimination.

5. If a constraint is encountered that isn't handled by the four preceding
rules, then it indicates a type error, and type inference fails.

Once all constraints have been examined, _Θ_ will contain constraints that
specify the actual type of each labeled expression in the program, including
the whole program itself. (Assuming that the program is correctly typed.)

When solving a linear system using Gaussian elimination, we may discover that
the system is over-constrained, or has no solution, or that it is under-
constrained, or has an infinite number of solutions. Similarly, type inference
may determine that the system specified by the constraints is over-constrained,
or under-constrained. An over-constrained system means that we have discovered
a type error, while an under-constrained system means that the program is
polymorphic, or can be satisfied by multiple types.

# Sources

[1]: http://cs.brown.edu/courses/cs173/2012/book/types.html
