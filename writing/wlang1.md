Language Analysis the First - Go
================================

Ben Draut / CS 330 / Oct. 15, 2013

# Introduction

_Go_ is a relatively new programming language that was developed by Google
beginning in 2007. Initially, it was built for the purpose of streamlining 
the development of Google's massive in-house systems. It was designed by and 
for people who work with large software systems.

# History

Most programming languages today that qualify as _system_ languages have been
around for a decade or more. Since the birth of these languages, the nature of
computer systems has changed significantly. Computer systems today are modern
marvels of networking and parallel processing. Cloud computing, which is so
ubiquitous today, was just a fantasy twenty years ago. Modern systems deal with
data sets that couldn't even fit on one computer, let alone be processed by
one. Yet they are still built using programming languages that were designed to
build systems that ran this way. Programming languages have not evolved at the
same speed that the computer systems have, which often leads to a frustrating
development experience. [1][2]

The speed of modern computers is tremendous, yet development was still
painstakingly slow at Google. The number of systems continued to grow, as did
the frequency of updates, but it seemed slow compile times and dependency
problems would forever be inescapable. Interpreted languages could be used
instead, but they too often sacrificed efficiency and safety for ease of use.
System engineers at Google, who worked with large-scale systems every day,
decided to start designing _Go_ in 2007 in order to address these concerns.

_Go_ was designed from the ground up to be a language that had everything the
system developers at Google wanted: Easy to use, clear, safe, portable, fast to
compile, and fast to execute. It had to support concurrency as a language
feature, so as to take advantage of networked computer clusters and multiple
processing cores. It would also have to be garbage collected, as much to
augment developer joy as to reduce developer error. Lastly, it had to be
concise. Everything ought to be defined just once.

With a vision in mind, Robert Griesemer, Rob Pike and Ken Thompson began
working on _Go_ part-time at Google in 2007. It became a public open source
project in 2009, and _Go 1_ was released on March 28, 2012 [3].

# General Features

## Type System

The _Go_ type system combines static and dynamic typing. It contains several
primitive types, which are statically checked, but also allows for dynamic
typing. It does not, however, support generic types. (Although they may be
added in future versions [4].)

_Go_ is also strongly typed, although a variable may be initialized without a
type declaration and _Go_ dynamically infers the type based on the value given.
However, once _Go_ decides the type of that variable, it persists, and the
variable must be used as if it were initially declared with that type. For
example, _Go_ will allow you to declare some variable _foo_ without a type and
give it a string value, but it is invalid to attempt to add _foo_ to a number.
(Though this is permitted in many weakly-typed languages and is interpreted as
string concatenation)

It is interesting to note that in _Go_, there are no implicit conversions for
numeric types. That is, if you attempt to assign the value in a 32-bit integer
variable to a 64-bit integer variable, the compiler will complain. The 32-bit
integer value must be explicitly cast as a 64-bit integer value. All numeric
type conversions in _Go_ must be explicit. Implicit numeric type conversions
complicate the compiler, and are not guaranteed to be portable. By requiring
this kind of conversion to be explicit, the compiler is simplified, and such
conversions become portable [5].

## Scope

_Go_ is lexically scoped, as are most modern programming languages. _Go_
delimits scoped regions by _blocks_. _Go_'s blocks are as follows, from the
most visible to the least visible: Universe, Package, File, Function.

## Objects

To the question "Is _Go_ object-oriented?" the creators respond "Yes and no."
Although it does support objects, the word object isn't typically used due to
implications that have been associated with it, but that _Go_ doesn't support.
_Go_ was designed to support both procedural and object-oriented programming
styles, but with some qualifications.

First, _Go_ does not support inheritance, so the inheritance-hierarchy way of
thinking doesn't translate well into _Go_. As such, _Go_ discourages the use of
terms such as "class", "object", and "instance" because they are so closely
associated with these ideas. _Go_ favors instead the terms "type" and "value",
where "_values_ of custom _types_ may have _methods_" [6]. 

Some may imagine that without inheritance, _Go_ must be repetitive and
redundant when dealing with similar types, but this isn't so. Take the classic
example of defining various shape types in inheritance-based object-oriented
languages, such as C++ or Java. We could define a _Circle_ class, which
inherits from the more general _Ellipse_ class, which inherits from the
abstract base class, _Shape_. On another branch of the hierarchy, we could also
have a _Square_ class, which would inherit from the more general _Rectangle_
class, which would also inherit from the _Shape_ class. Now each class can
define it's own methods such as _Perimeter_ and _Area_ that do the appropriate
math for that kind of shape. Now, anywhere we do anything with Shapes, we only
have to write one piece of code that will work for all descendants of _Shape_.

This may appear to be an acceptable approach, and in some cases it is. _Go_,
however, behaves a bit differently. Going back to our example, in _Go_ we would
define an _interface_ called _Shape_. The interface would have method
signatures for _Perimeter_ and _Area_. Now, we start defining our different
_types_ to represent different kinds of _Shapes_. As long as these _types_ have
methods that match the signature of the methods in the _Shape_ interface, _Go_
will automatically recognize them as "kinds" of _Shapes_, and allow you to use
them anywhere that the _Shape interface_ is used. This is also known as  _duck-
typing_, and it eliminates many of the headaches that large inheritance
hierarchies bring with them. (Increased complexity, code bloat) Many experts,
including those at Google, feel that inheritance actually creates more problems
than it solves [7]. _Go_'s lightweight type system is one of the greatest
factors in it being a productive language.

Duck-typing is not without its own problems unfortunately. Interfaces in _Go_
are structural, meaning that _Go_ decides whether or not a type conforms to an
interface by checking to see if the interface's methods are a subset of the
type's methods. So it is possible for a type to conform to an interface that
doesn't really describe the type. We could imagine two interfaces, that have
similar method signatures, but represent in reality very different things. A
type that implements all the methods would conform to both interfaces, but it
may not make sense for a value of the type to be used where one of the
interfaces is specified. This could be misleading to a programmer were it
discovered, and may perhaps be an argument against this methodology. However,
developers will likely only know that their types conform to interfaces that
they expect them to conform to, and will use them as such. Being unaware of
additional interfaces that their types conform to detracts nothing from their
program.

At first, it may appear that without inheritance programmers are forced to
repeat code when defining similar types. Every type of shape in our example
would have to define its own fields for position, or any other attributes
common to all shapes. Isn't that a step backwards? Although _Go_ doesn't
support inheritance, it does resolve some of these issues by still supporting
aggregation and embedding.

Aggregation is familiar to us, and is what one would expect in _Go_. Types may
have other types as fields in their definition. In our shape example, we could
create a Point type, (to represent the position of a shape) and each of our
different shapes would have a Point field. This works, but is not necessarily
the best solution, as forces message chaining and can reduce the clarity of our
code. Embedding, on the other hand, is done the same way as aggregation, but
the field declaration is anonymous. In the shape example, we would declare the
Point field in each type, but not give it a name. This would allow us to use
_rect.X_ as opposed to _rect.Point.X_. Embedded fields behave as if whatever
that field contains were declared in the type to begin with. The only time that
the type name is needed is to disambiguate between fields of the type itself,
and fields of the embedded type.

Another interesting design decision that the designers of _Go_ made was how to
handle the visibility of fields in custom types. Rather than add additional
keywords or require some kind of grouping in the type definition, they decided
to adopt a simple rule: Fields beginning with a lowercase letter will not be
exported, (private) while fields beginning with an uppercase letter will be
exported. (public) The designers say this was a difficult decision to make, but
they are extremely pleased with it now. It greatly increases the clarity of Go
code, as the scope of fields is inherently identified in their name [8].

### Memory Allocation and Garbage Collection

The creators of _Go_ felt that manual memory management required too much
programmer overhead and decided to implement garbage collection in _Go_. This
is especially true when writing concurrent applications that share data and
memory between threads or processes. (For which _Go_ was designed) It can be
extremely difficult to know for sure when resources can be released. _Go_'s
philosophy is that it's better for the language creators to implement this
once, rather than programmers having to worry about it for each application.
This way, developers can take full advantage of _Go_'s concurrency mechanisms
without needing to worry at all about managing resources.

_Go_'s philosophy also encourages thinking simply about _memory_, rather than
distinguishing between stack space and heap space. To the programmer, memory
should simply be memory, with no extra bookkeeping required. That being said,
they recognize that in reality, whether values are allocated on the stack or
the heap does have a very real impact on performance, and the _Go_ compiler has
been designed to make effective choices about where to allocate memory. In
addition, the programmer may reduce garbage collecting overhead significantly
by using the language properly [8].

Documentation states that currently a concurrent mark and sweep model is used,
although that may change in future releases of _Go_ [8].

# Special Features

## Package System

One of the primary objectives of _Go_ was efficiency, both in compilation and
execution. _Go_'s fast compilation speeds are primarily due to the nature of
its package system. In C++, build times grow tremendously as more and more
files are added, primarily due to the nature of the preprocessor and how
dependencies are handled. Build engineers at Google tracked the workings of the
preprocessor in compiling one of Google's binaries, and discovered that,
although all source files together totaled a mere 4.2 megabytes, over 8
gigabytes were flowing from the preprocessor to the compiler [9]. _Go_ handles
dependencies using its package system, which has been designed from the ground
up to be as efficient as possible. Some of the distinctions that set it apart
are that unused packages cause a compiler error, thus ensuring that only what
is needed and used is imported, and that _Go_ packages compile to object files
that contain everything required for client packages. This means that package B
may import packages C through Z, but for package A to use package B, the
compiler needs only read package B's object file. This helps build times
significantly.

## Concurrency Mechanisms

Another primary goal of _Go_ was to provided explicit support for concurrency,
thus enabling developers to more easily take advantage of the hardware in
modern multi-core and networked systems. The fundamental concurrency tools in
_Go_'s toolbox are _Goroutines_ and _Channels_.

Goroutines may be thought of as extremely lightweight threads or processes, but
the language manages all the details in the OS. _Go_ has been designed to
support thousands of Goroutines in single programs. Channels are the mechanisms
by which Goroutines coordinate and communicate with each other.

Channels are values that pass around other values. For example, you could
create a channel of integers. Two or more Goroutines may then communicate by
sending values across this channel.

Channels are also first-order values in _Go_, meaning that it is valid to
create a channel of channels. This is incredibly useful for communicating
between Goroutines. Goroutine A may send a request to Goroutine B over a
channel, with a channel embedded in the request that Goroutine B can use to
signal to Goroutine A the response or result of its request. This makes writing
safe remote procedure calls extremely easy. Channels as first-order values is
hailed as one of the greatest features of the language.

Because _Go_ also has first-order functions and closures, there are some
interesting mistakes that can be made when using these features with
concurrency mechanisms. Imagine we have a slice of strings: "byu", "cs", and
"330". We could loop through the slice, print each string out, and see the same
list, just as we would expect. Now let's do something that seems silly for this
case, but could apply to a similarly framed real-world situation. Rather than
just printing each string, let's call an anonymous function that prints it for
us. The result is the same. Now, rather than calling the anonymous function
directly, let's call it with the go keyword to make it a Goroutine. If you use
the loop variable to print each string, this will output "330", "330", and
"330". This happens because all three closures in the anonymous functions share
the same loop variable. In most situations (single core machines), the launch
of the Goroutines will be deferred until the loop completes. When the loop
completes, the loop variable will have the value of the last item in the list,
thus causing the repetition in the output. To get around this, a new variable
must be created in each iteration of the loop and given to each function. (This
is a common idiom in _Go_) So, _Go_ does provide excellent tools for
concurrency, but care must be taken when using them. Thankfully, _Go_ ships
with a tool that can detect many of these kinds of mistakes. (go vet)

## Error Handling

_Go_'s philosophy towards error handling takes a somewhat unique approach. _Go_
does not provide support for assertions or exceptions, arguing that they
needlessly complicate code, while simpler alternatives using already available
language features are preferable.

_Go_'s model for handling errors is simply that. To _handle_ them. Exceptions
encourage programmers simply to pass them up the call stack, let someone else
worry about them. _Go_ makes it extremely easy on the other hand, to deal with
errors, if somewhat more verbosely. Although a tiny bit of extra code is
required, it simplifies the code overall by reducing the number of logical
paths the code could follow.

Although _Go_ does not support exceptions in the traditional sense, it does
have support for similar mechanism, known as _panicking_. Most developers are
comfortable with the try-catch-finally model. Code that could throw an
exception is surrounded by a try block, followed by a catch block (blocks) to
catch and handle different exceptions that could occur, followed by a finally
block that is guaranteed to be executed, typically used for cleanup. Developers
of _Go_ felt that this model complicated code needlessly, and had become
abused. Too often developers were using exceptions for control flow and dealing
with simple errors. Truly exceptional situations are far rarer that most
developers think. _Go_ uses a defer-panic-recover model. Calling defer on a
function will push it onto a stack, from which all functions are popped and
executed when the function ends, or if a panic occurs. Panics may be initiated
by the language for things like trying to access an invalid array index. When a
panic is triggered, all deferred functions are executed up the call stack until
either the program terminates, or recovers. Deferred functions may call
recover, which will stop the panicking, return the value of the last panic, and
resume normal execution.

This model has several benefits. First, panicking aside, being able to defer
functions helps improve the readability of the code and makes life easier on
the developer. Whenever the developer creates a resource or does any kind of
set up, they can think about the tear down at the same time, and just defer it.
This makes it really easy to make sure that things get cleaned up. As soon as
you open a file, defer the function that will close it. Done. If a panic does
occur, you're still guaranteed that it will get cleaned up. It also simplifies
the logic of handling exceptional situations.

There are some interesting issues that this model introduces as well however.
What if a function panics, and then one of its deferred functions panics too?
Not much changes. All deferred functions are still executed up to the point
where they panic, at which point the next deferred function will be executed,
or execution will jump up the call stack. This can cause problems though, as
recover only remembers the reason for the last panic, not the whole chain, so
it can be easy to obscure the real reason that panics happen. Say you have a
function that causes a panic. Imagine you tried to access an out-of-bounds
array index. Execution jumps to the deferred functions where you do some
cleanup. Now, another panic happens because something went wrong during the
cleanup. There are a few possible scenarios: First, you don't do any recovery,
and both panics are displayed in the program output. Now you're aware of both,
which is good! Second, you attempt to recover often, so you're able to detect
and recover from the first panic before the second happens. Good! Third, you
attempt to recover, but not often. You're program recovers before it
terminates, and it looks like it was because of the cleanup panic. You have no
idea that the panic in the original function ever happened. If programmers
aren't careful about how and where they do panic recovery, it could make
debugging significantly more difficult.

Another aspect of this model that is worth considering is how recoveries
actually work. Recover may only be called in a deferred function. If it is
called anywhere else, it will just return nil, and have no effect on the
program. Deferred functions are only executed when execution is leaving a
function though, so there is no way to return to the code in a function after
recovering from a panic. With the try-catch model, it is normal to simply
surround potentially problematic segments of code, thus allowing you to protect
the program from terminating, while still allowing you to finish the code
block. In _Go_, this isn't possible. As soon as panic is called, the rest of
that function is gone. You can recover in the first deferred function, but
execution will continue at the next level up the call stack. There are
arguments for an against this. My opinion is that it encourages programmers to
write concise functions, which simplify code and improve clarity.

## Safety Features

The designers of _Go_ implemented several features to improve the safety of the
language. A few of these are that all variables are guaranteed to be
initialized to 0 or nil, if not explicitly initialized on creation and no
pointer arithmetic is allowed [10].

An interesting feature to note is how _Go_ handles memory. In most languages,
returning the address of a local variable on the stack from a function is a big
mistake. As soon as the function terminates, the variable goes out of scope, is
released, and all of a sudden you have a wild pointer on your hands. Not so in
_Go_. One of the features of _Go_ is that it will preserve stack variables that
have had their addresses taken so as to prevent this problem.


# Sources

[1]: http://golang.org/doc/faq#Origins
[2]: http://talks.golang.org/2012/splash.article
[3]: http://golang.org/doc/faq#What_is_the_status_of_the_project
[4]: http://golang.org/doc/faq#generics
[5]: http://golang.org/doc/faq#conversions
[6]: Summerfield, _Programming in Go_, 254
[7]: http://asserttrue.blogspot.com/2009/02/inheritance-as-antipattern.html
[7]: http://talks.golang.org/2012/splash.article#TOC_11.
[8]: http://golang.org/doc/faq#garbage_collection
[9]: http://talks.golang.org/2012/splash.article#TOC_5.
[10]: http://talks.golang.org/2012/splash.article#TOC_12.

