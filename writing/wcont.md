Analysis: Continuations
=======================

Ben Draut / CS 330 / November 13, 2013

# Introduction

Computer programming languages contain instructions that transfer control of a
program to another section of code. For example, the return keyword in most
languages transfers control from the function in which it is called back up to
the calling function. These instructions are called control operators.

A typical program in language such as Java or C consists of calling functions
to compute desired outputs or results. When a function is called, control of
the program is transferred to the function, and the function transfers control
back to the calling function when it has finished. It is often helpful to think
about how the call stack looks when the program is running in order to
understand it better. We know that when a function is called, a new stack frame
is created with the necessary data. We also know that when a function
terminates, its stack frame is popped off the call stack, and control returns
to the calling function. The transfer of control is implicit in the return of
the function. I will use the term call-return style to refer to this method of
control.

Continuation-passing style introduces an alternative method of transferring
control between parts of a program. Unlike call-return style, in continuation-
passing style, the transfer of control is always explicit. In a program written
in continuation-passing style, all functions take an additional argument. This
argument is itself a function that takes a single argument. This function is
the continuation, and it takes as its argument the result of the previous
function. The last thing that any function in a continuation-passing style
program does is call the continuation function with its result. The transfer of
control is explicit in the calling of the continuation.

We can think about the difference between these two styles in another way. If
we pick some point in a program, we can identify the computation that has
already been performed, and the computation that has yet to be performed, or
the rest of the computation. In call-return style, the rest of the computation
is the stack. The program progresses as control is transferred back up the call
stack. In continuation-passing style, the rest of the computation is in the
continuation. The program progresses as continuations are invoked.
Conceptually, the program flow follows a linear structure rather than a stack
structure.

# Utility

One example of how continuation-passing style can be useful is in writing web
applications. Whenever one navigates to a URL in their browser, an HTTP request
to a server is issued for that page. The server responds to the request, and
then closes the connection. If the same user requests another page from the
same server, the server has no idea that it has seen that user's requests
before. In other words, HTTP is stateless. The is no provision for any sort of
state or memory in the protocol itself. What does this mean? It means that
websites must embed some sort of identifying data in the request/response flow
in order to distinguish requests from one client from another. These usually
take the form of cookies or hidden form fields. When a subsequent request is
issued to the server, it receives the cookie or the values in the hidden fields
and uses them to tell which client sent the request. This approach works, but
it requires additional bookkeeping, which burdens the developer with extra work
that doesn't directly fulfill the purpose of the website. 

Racket provides useful mechanisms for eliminating the need for this by using
continuation-passing style. The Racket web server has been built to support the
concept of state, even over a stateless protocol. When a client issues a
request, it opens an interaction with the server. The server can send a
response to the client, and then "suspend" the interaction until a new request
is received from the client. It does this by capturing the continuation, and
then binding it to a URL. When the client sends information back in a new
request, it can send it to the continuation-bound URL. "If the continuation URL
is invoked, the captured continuation is invoked and the request is returned
from this call to send/suspend." [1] This feature allows a developer to write a
web application that requires interaction from the user as if it were a regular
program that blocked whenever waiting for user input. No need to worry about
accessing a client's data in a session or database, it's just there!

There are other situations in which continuation-passing style is useful, two
of which I have encountered before without realizing I was using continuations.
The first is when issuing AJAX calls to a server from a web page, and the
second is when using callbacks as part of a GUI (Graphical User Interface).
AJAX is an acronym for Asynchronous Javascript and XML. It is used to issue a
request to a server and receive a response programmatically in Javascript. In
my experience, I have used AJAX to fill in data once a page has loaded, or to
cause an action on the server without forcing the client to navigate to a new
page. As its name denotes, AJAX requests are issued in Javascript code.
Javascript is single- threaded, so if AJAX requests were blocking, then the
client's browser would appear to freeze while waiting for an AJAX call to
complete. Clearly this is undesirable, but how is it remedied? Whenever an AJAX
request is issued, the programmer also supplies a callback function, or a
function that will be executed when the operation is complete. It could be a
function that simply makes sure that the request was successful, or it could be
a function that updates the DOM of the web page using the data received from
the server. This callback is an explicitly passed continuation.

The concept is the same in GUI programming, only without the server. In GUI
applications, there is no predetermined sequence of events. The user can click
any button, fill or clear any text field, or interact with any other component
at any time. To handle this, callback functions are registered with various GUI
components to be executed when the component is triggered. For example, when a
button with a callback function is pressed, its callback will be executed. Just
as in the AJAX example, this callback function is an explicitly passed
continuation.

# Power of CPS

It is interesting to note that although continuation-passing style can make
writing some programs much easier than it would otherwise be, it does not
increase the power of a language. We still have languages that are Turing-
complete without control operators in general, and so they (including
continuations) cannot increase the language's "power" in what it can or cannot
compute [2]. As we have seen however, it can increase a language's "power" in
the sense that it can make some programs easier to write, or express the
developers intent more clearly.

# Tail Call Optimization and CPS

When a function calls another function as its final action, that call is known
as a tail call. Tail calls are interesting to us because they allow for a
particular optimization. If calling a function is the last action that a
function does, then there is no need to create a new stack frame for the call.
The tail call can use the same stack frame as the parent function, since the
only important information left is its return value. (Which is the result of
the second function.) This is known as tail call optimization.

This is useful because it allows any number of tail calls to be made in
constant space. As an example, we'll look at tail recursion. Tail recursion is
a special case where a function calls itself recursively in a tail call.
Without tail call optimization, it is possible that tail recursion could run
out of space and cause a stack overflow, when in reality the majority of data
on the stack will never be needed again. The possibility of causing a stack
overflow is also there in regular recursion, however the stack must be
preserved in regular recursion so as to allow each function call to complete
when it returns from the recursive call. With tail call optimization and enough
stack space for at least one call, tail recursion can't cause a stack overflow,
because each recursive call takes the stack from of its parent function. When
the recursion finally terminates, the result is returned directly without
having to propagate up the stack.

Tail call optimization is also interesting to us because it enables us to use
continuation-passing style without needing to worry about causing a stack
overflow. Remember, a function written in continuation-passing style calls
another function as its final action, so it can be benefited by tail call
optimization. For example, we can imagine a call-return style program that runs
to completion successfully, but when converted to continuation-passing style,
causes a stack overflow. In a call-return style program, we know that the call
stack is likely going to grow and shrink as the program executes. In a program
that is written exclusively in continuation-passing style however, the call
stack only grows. This is because every function calls another function before
it finishes, which creates a new stack from unless tail call optimization is
supported. It it possible in a call-return style program that the call stack
will never shrink until the program finishes and returns, but this is unlikely.
(Were this true, then the call-return style program would cause a stack
overflow in our example as well.) With tail call optimization, our
continuation-passing style program will require even less space than the call-
return style program because each time the continuation is called, it replaces
the current stack frame with its own.

# Beating the Averages

I found Paul Graham's article very interesting. It caused me to think a lot
about the way that I view programming languages not only when comparing them to
other languages, but also the way that I think about and use my "favorite"
languages. Paul talks about what he calls the Blub paradox, where he talks
about how it is usually easy for developers to recognize languages that are
less powerful than their favorite language, but much more difficult to
recognize languages that are more powerful. I identified with this very
strongly. When I compare programming languages, I usually catch myself thinking
that all of them are capable of doing the same things, so why shouldn't I just
stick with one that I know well already? I realize now that when I see
languages with more powerful features, I pass them off as interesting, but
unnecessary. I also realized however that I don't often do the reverse that
Graham mentions, that it, I don't often find myself looking down on less
powerful languages because of features they lack. I just view them as equal
alternatives.

In my mind, this means one of two things. The first is that my favorite
languages are, in fact, the least powerful languages around, so there is
nothing to look down on. After some thought, I don't think that this is the
case. What I think is actually true is that I have struggled to develop enough
mastery of any one language to correctly identify and use the features that set
it apart from other languages. Reading this paper made me realize that I have
trained myself to program in such a way that I don't use, rely on, or need to
learn, the advanced features of a language. The majority of programs that I
write, though they may be large, rely on very simple constructs that are common
to just about every programming language. This was slightly depressing to
realize. Though I may say that I "know" a language, what that actually means is
that I "know how to declare variables, write loops and call functions." Reading
this paper inspired me to want to learn better ways of programming, rather than
just making things work. It made me want to develop the skills to be able to
see the differences in languages, and really understand when to use its more
advanced features.

As I explored some of the other articles that Graham's article linked to, I
came across a blog post written by Joel Spolsky entitled "The Perils of
JavaSchools." [3] In this post, Spolsky explores some of the concepts that make
Computer Science difficult, and then talks about how universities are really
doing a disservice to their students by trying to make their computer science
programs easier. He says that to really be good programmers, students need to
learn to be comfortable simultaneously thinking at different levels of
abstraction, being able to understand pointer manipulation and recursion for
example. As he interviews computer science graduates for jobs, it's more
difficult to tell if they have those skills when they have written Java code
for the majority of their program. At one point in his article, he links to a
quiz [4] containing a few questions from one of his college midterms. Its
designed for you to test yourself to see if you're comfortable with the skills
he has been talking about. I took this quiz, and did well for the most part,
but it still took me longer than I would have liked. I started to think about
how I have become lazy in some aspects of programming. I know have solved
problems like these in the past, so I don't put as much thought into them now.
I tend to use Google or just guess far more often than I used to. Both of these
articles opened my eyes to the immense progress that I still have to make.

# Sources
[1]: http://docs.racket-lang.org/web-server/servlet.html
[2]: http://cs.brown.edu/courses/cs173/2012/book/Control_Operations.html
[3]: http://www.joelonsoftware.com/articles/ThePerilsofJavaSchools.html
[4]: http://www.joelonsoftware.com/articles/TestYourself.html
