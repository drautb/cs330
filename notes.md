CS 330 Notes
============

# Tuesday (Friday Schedule) 11/26/13

* 1 Trivia Point (Not Redeemed)

# Wednesday 11/14/13

* 2 Trivia Points (Not Redeemed)

# Monday 10/27/13

* 1 Trivia Point (Not Redeemed) 

# Wednesday 10/23/13

* 1 Trivia Point (Not Redeemed)

# Wednesday 10/9/13

* 1 Trivia Point (Not Redeemed)

# Monday 10/7/13

* 1 Trivia Point (Not Redeemed)

# Wednesday 10/2/13

* 3 Trivia Points (Not Redeemed)


# Wednesday 9/25/13

* 1 Trivia Point (Not Redeemed)

**Scope**
* _Dynamic Scoping_
    * Basically, all variables are visible from any part of the program.
* _Lexical_ or _Static_ _Scoping_
    * The kind of scope you're used to, new environment for each function call.

* It is common to have _dynamic scope_ at the top-level, to allow for some form of global variables, but at any sublevel, it is _lexically scoped_.


# Monday 9/9/13

## Higher-Order Functions

**Property Definitions**
* **_pure:_** One call does not affect another call. Map +1 on a million numbers can be done in parallel.
* **_associative:_** Any parenthization of the operation gives the same answer. Things can be brought back together in any order.

### map

The arguments to **map** must be **_pure_**, meaning that they don't affect one another, so every single operation can happen in parallel.

### foldr (reduce in python)

If the argument to **foldr** is **_associative_**, then the operations can be parallelized, because they can be re-combined in any order.


# Miscellaneous

# Notes for myself/Stuff to address

# Common Stuff First
    # Types
    # Object
    # Scope
    # Memory Management

# Supported Features
    # Closures
    # Functions
    # ?

# Cool Stuff

    # Package System
    # Concurrency Stuff
    # Saftey features http://talks.golang.org/2012/splash.article#TOC_12.

* Deallocating memory used for code that is no longer needed. (Deallocating
memory for a class that will never be needed again.)
    * Garbage Collection

* Objects
* Functions (first-order?)
* Type System
* Scope
* Memory Model
* Garbage Collection
* Cool Features
    * Concurrency features
    * Goroutines
* Missing Features
    * Generic Types
    * Exceptions
    * Assertions

http://golang.org/ref/spec#Declarations_and_scope

* Can't define methods on built in types....


# Notes for Type Systems Paper

* What is a type?
    * A Type is an invariant. It's a guarantee that certain things will always be true of x or y. Things like size, operations tha will work, values that can be stored in it, which operations can be combined, etc.
    * Types are one kind of invariant that you may want in your program.
    * Ex: integers that are only positive, list will always be sorted, etc.

* What is the purpose of type systems? What do they do?
    * Type systems are one way of enforcing a small set of invariants.
    * To specify invariants so that you as a human can rely on these things always being true in your program.
        * The compiler and runtime can make use of this information too, but they are there for YOU.

* We can think of programs as being good or bad, based on whether or not they obey the invariants specified by the type system.

* What are the different kinds of type systems?
    * Static? Dynamic? Safe? Inferred? etc?

* Static means at compile time. In contrast to Dynamic, which is at runtime.
    * Static vs Dynamic enforcement of invariants. Not the same as "Dynamically typed"

* Errors
    * Logic Errors (Divide by zero)
    * Type Errors (Are you performing the allowed operations)
    * Syntactic Errors ()

    * It's kind of hard to distinguish between the power of logic errors and type errors. It's very difficult to make a type system specific enough to cover all logic errors.

* When designing a type system, think about the kinds of values you have in your language. (Numbers, closures, etc.)

* Type systems verfiy ALL code, even if it will never be run! Great strength, but also a great cost.


Functional Programming in C++
* It is possible
* Extra benefits. Encapsulated, no side effects, thread safety, easy to parallelize, etc.


Progress: If e type checks, then you can run e at least one step. You can do work.
Preservation: If it has a type, and it runs, then the resulting type is the same as the type of the original expression.

Safety: Means that your program will not perform incompatible operations. Safety is similar to having a type system, but it checks things dynamically.

Dynamic type system = Language is safe

If safety is provided statically, that is a type system.

You can have both or neither. They are orthogonal ideas. 

Safety is the same as latent types. 